import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article} from '../model/Article';
import {Tag} from '../model/Tag';
import {User} from '../model/User';
import {Categorie} from '../model/Categorie';
import {NewArticle} from '../model/NewArticle';

@Injectable()
export class ZBlogService {
  constructor(private http: HttpClient) {
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('http://localhost:8080/articles');
  }

  getArticle(id: string): Observable<Article> {
    return this.http.get<Article>(`http://localhost:8080/articles/${id}`);
  }

  getTag(id: string): Observable<Tag> {
    return this.http.get<Tag>(`http://localhost:8080/tags/${id}`);
  }

  getUser(id: string): Observable<User> {
    return this.http.get<User>(`http://localhost:8080/users/${id}`);
  }

  getCategorie(id: string): Observable<Categorie> {
    return this.http.get<Categorie>(`http://localhost:8080/categories/${id}`);
  }


  createArticle(article: NewArticle): Observable<Article> {
    return this.http.post<Article>(`http://localhost:8080/articles/`, article);
  }

  getAllTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>(`http://localhost:8080/tags/`);
  }

  getAllCategories(): Observable<Categorie[]> {
    return this.http.get<Categorie[]>(`http://localhost:8080/categories/`);
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(`http://localhost:8080/users/`);
  }

  deleteArticle(id: string): Observable<Article> {
    return this.http.delete<Article>(`http://localhost:8080/articles/${id}`);
  }
}
