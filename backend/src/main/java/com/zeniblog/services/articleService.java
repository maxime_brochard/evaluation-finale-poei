package com.zeniblog.services;
import java.lang.String;

import com.zeniblog.repository.articleRepository;
import com.zeniblog.repository.tagRepository;
import com.zeniblog.services.domain.*;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

@Component
public class articleService {
    private articleRepository articleRepo;
    private tagService tagService;
    private UserService userService;
    private categorieService categorieService;
    private idGenerator idGenerator;

    public articleService(articleRepository articleRepo, com.zeniblog.services.tagService tagService, UserService userService, com.zeniblog.services.categorieService categorieService, com.zeniblog.services.idGenerator idGenerator) {
        this.articleRepo = articleRepo;
        this.tagService = tagService;
        this.userService = userService;
        this.categorieService = categorieService;
        this.idGenerator = idGenerator;
    }

    public List<Article> getAllArticles() {
        return (List<Article>) this.articleRepo.findAll();
    }


    public Article createArticle(NewArticle toCreate) {
        String futureArticleId = idGenerator.generateNewId();

        List<Tag> tagList = new ArrayList<>();
        for (String tagId : toCreate.getTagList()) {
            Optional<Tag> maybeTag = this.tagService.getTagById(tagId);
            maybeTag.ifPresent(tag -> tagList.add(new Tag(
                    tagId,
                    maybeTag.get().getName()
            )));
        }

        Categorie cate = this.categorieService.getCategorieIfIdIsPresent(toCreate.getCategorie_id());
        User author = this.userService.getUserIfIdExist(toCreate.getAuthor_id());

        String[] abstractWordsArray = toCreate.getArticleAbstract().split("\\s+");

        if (abstractWordsArray.length > 50) {
            throw new IllegalArgumentException("Invalid process, the abstract cannot contain more than 50 words");
        }

        return this.articleRepo.save(
                new Article(
                        futureArticleId,
                        toCreate.getTitle(),
                        toCreate.getContent(),
                        toCreate.getArticleAbstract(),
                        toCreate.isStatus(),
                        cate,
                        author,
                        tagList));
    }

    public Optional<Article> getArticleById(String id) {
        return this.articleRepo.findById(id);
    }

    public Optional<Article> deleteArticle(String id) {
        Optional<Article> article = this.articleRepo.findById(id);
        if (article.isPresent()) {
            this.articleRepo.deleteById(id);
            return article;
        }
        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent article");

    }
}
