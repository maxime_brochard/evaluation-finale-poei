import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ZBlog';
  @Input() showCreateArticle = false;
  @Input() showCreateArticleButton = true;
  timestamp: EventEmitter<boolean> = new EventEmitter();

  showSubmitValue: boolean;
  submitValue: boolean;
  submitMessage: string;


  toogleArticleCreator() {
    this.showCreateArticle = !this.showCreateArticle;
    this.showCreateArticleButton = !this.showCreateArticleButton;
  }
  updateList() {
    this.timestamp.emit(true);
  }

  toggleSubmitValue($event) {
    if ($event) {
      this.submitMessage = 'L\'article à bien été créé';
    } else {
      this.submitMessage = 'Erreur, l\'article n\'a pas pu être créé';
    }
    this.submitValue = $event;
    this.showSubmitValue = true;
    this.updateList();
  }

}
