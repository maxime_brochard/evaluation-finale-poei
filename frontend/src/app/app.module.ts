import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { ListArticlesComponent } from './list-articles/list-articles.component';
import {ZBlogService} from './service/ZBlogService';
import { ArticleComponent } from './article/article.component';
import { ArticleCardComponent } from './article-card/article-card.component';
import {RouterModule, Routes} from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import {FormsModule} from '@angular/forms';
import { DeleteArticleComponent } from './delete-article/delete-article.component';

const appRoutes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  {path: 'home', component: ListArticlesComponent},
  {path: 'article/:id', component: ArticleComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    ListArticlesComponent,
    ArticleComponent,
    ArticleCardComponent,
    HeaderComponent,
    CreateArticleComponent,
    DeleteArticleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
  ],
  providers: [ZBlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
