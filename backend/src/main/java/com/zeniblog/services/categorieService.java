package com.zeniblog.services;

import com.zeniblog.repository.categorieRepository;
import com.zeniblog.services.domain.Categorie;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class categorieService {
    private categorieRepository categorieRepo;

    public categorieService(categorieRepository categorieRepo) {
        this.categorieRepo = categorieRepo;
    }

    public List<Categorie> getAllCategories(){
        return this.categorieRepo.findAll();
    }

    public Optional<Categorie> getCategorieById(String id) {
        return this.categorieRepo.findById(id);
    }

    public Categorie getCategorieIfIdIsPresent(String id) {
        Optional<Categorie> maybeCate = this.getCategorieById(id);
        if (maybeCate.isPresent()) {
            return new Categorie(maybeCate.get().getId(), maybeCate.get().getName());
        }
        throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent categorie");
    }
}
