import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Article} from '../model/Article';
import {ZBlogService} from '../service/ZBlogService';

@Component({
  selector: 'app-delete-article',
  templateUrl: './delete-article.component.html',
  styleUrls: ['./delete-article.component.css']
})
export class DeleteArticleComponent implements OnInit {
  @Input() article: Article;
  @Output() deletedArticle = new EventEmitter();

  constructor(private zService: ZBlogService) { }

  ngOnInit() {
  }

  deleteArticle(){
    return this.zService.deleteArticle(this.article.id).subscribe(
      () => {
        this.deletedArticle.emit(true);
      }
    );
  }

}
