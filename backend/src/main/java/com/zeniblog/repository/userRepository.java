package com.zeniblog.repository;

import com.zeniblog.services.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface userRepository extends JpaRepository<User, String> {
}
