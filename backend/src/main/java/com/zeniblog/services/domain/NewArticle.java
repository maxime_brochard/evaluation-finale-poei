package com.zeniblog.services.domain;

import java.util.List;

public class NewArticle {
    private String title;
    private String author_id;
    private String articleAbstract;
    private String content;
    private boolean status;
    private String categorie_id;
    private List<String> tagList;

    public NewArticle(String title, String author_id, String articleAbstract, String content, boolean status, String categorie_id, List<String> tagList) {
        this.title = title;
        this.author_id = author_id;
        this.articleAbstract = articleAbstract;
        this.content = content;
        this.status = status;
        this.categorie_id = categorie_id;
        this.tagList = tagList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public String getArticleAbstract() {
        return articleAbstract;
    }

    public void setArticleAbstract(String articleAbstract) {
        this.articleAbstract = articleAbstract;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(String categorie_id) {
        this.categorie_id = categorie_id;
    }

    public List<String> getTagList() {
        return tagList;
    }

    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }
}
