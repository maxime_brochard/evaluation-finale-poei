package com.zeniblog.controllers;

import com.zeniblog.controllers.representations.ArticleFullRepresentation;
import com.zeniblog.controllers.representations.ArticleMapper;
import com.zeniblog.controllers.representations.ArticleShortRepresentaiton;
import com.zeniblog.services.articleService;
import com.zeniblog.services.domain.Article;
import com.zeniblog.services.domain.NewArticle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/articles")
public class articleController {

    public articleService articleService;
    public ArticleMapper mapper;

    @Autowired
    public articleController(com.zeniblog.services.articleService articleService, ArticleMapper mapper) {
        this.articleService = articleService;
        this.mapper = mapper;
    }

    @GetMapping
    List<ArticleShortRepresentaiton> getAllArticles() {
        return this.articleService.getAllArticles().stream()
                .map(this.mapper::mapToShortArticle)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    public ResponseEntity<ArticleFullRepresentation> getOneArticle(@PathVariable("id") String id){
        return this.articleService.getArticleById(id)
                .map(this.mapper::mapToFullArticle)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public Article createArticle(@RequestBody NewArticle body){
        final Article toBeCreatedArticle;
        if (body != null){
            toBeCreatedArticle = this.articleService.createArticle(body);
        } else {
            throw new IllegalArgumentException("Please provide a body");
        }
        return toBeCreatedArticle;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ArticleShortRepresentaiton> deleteArticle(@PathVariable("id") String id){
        return this.articleService.deleteArticle(id)
                .map(this.mapper::mapToShortArticle)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
