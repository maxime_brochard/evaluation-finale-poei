drop table if exists tags_assignements;
drop table if exists articles;
drop table if exists categories;
drop table if exists tags;
drop table if exists users;


create table categories
(
	id text primary key,
	name text not null
);

create table users
(
	id text primary key,
	pseudo text not null,
	mail text not null
);

create table articles
(
	id text primary key,
	title text not null,
	content text not null,
	abstract text not null,
	status boolean not null,
	categorie_id text REFERENCES categories (id),
	author_id text REFERENCES users (id)
);

create table tags
(
	id text primary key,
	name text not null
);

create table tags_assignements
(
    article_id text REFERENCES articles (id),
    tag_id text REFERENCES tags (id)
);



insert into users values ('08139ffb-76f0-4a2e-973f-85218f0d3fc7', 'Max', 'max.zenika@mail.com');
insert into users values ('3a606172-3dc7-4fa2-940a-393e1e9a1b5c', 'Cyn', 'cyn.zenika@mail.com');
insert into users values ('f2d9db5c-d41a-4675-ba33-3d7e522ae38d', 'Jo', 'jo.zenika@mail.com');
insert into users values ('ef71c247-4415-4745-a8b1-71a6a9ac4253', 'P.A', 'pa.zenika@mail.com');
insert into users values ('c7fb5f37-e3c1-4887-8c82-558e1d0759ab', 'Ritchie', 'ritchie.zenika@mail.com');
insert into users values ('04caf097-b8b9-4844-b00c-3fffb1d9492f', 'Erwan', 'erwan.zenika@mail.com');
insert into users values ('72ee12bf-57ab-4ca2-aff1-92feed83bb02', 'Marion', 'marion.zenika@mail.com');
insert into users values ('c8b3a325-5af4-43c3-87d6-14bc94a7b1df', 'Baptiste', 'baptiste.zenika@mail.com');
insert into users values ('85c9ab49-d19a-4fba-a33a-dfd67111e4dd', 'Sarah', 'sarah.zenika@mail.com');
insert into users values ('82d9b743-bba9-4e6d-8a2e-1d78aa8d7c7b', 'Aurelien', 'aurel.zenika@mail.com');
insert into users values ('861f11a1-8adc-44cf-8ba0-100b1779e624', 'Charles', 'charles.zenika@mail.com');
insert into users values ('a5e2358a-542b-4da0-b085-8eb4eeb19541', 'Stephane', 'steph.zenika@mail.com');
insert into users values ('f0cfb4be-8227-44ad-ab42-8329ac927fcf', 'Benoit', 'benoit.zenika@mail.com');

insert into categories values ('91b1e88a-fbf9-4d0e-ac31-0272c81aceee', 'Sport');

insert into tags values ('99abcec4-77ac-4a61-b0ec-6f097f0f4928', 'sport');
insert into tags values ('84104992-e0c9-42f1-b2cc-aa0e7fee0ce8', 'IoT');
insert into tags values ('516493fb-32ba-479b-b090-4a90cbad1666', 'geek');
insert into tags values ('86bf50fc-916a-4991-9ad3-f88a93c2db88', 'engineering');

insert into articles values ('1a335e9d-c74f-44fc-9c65-9b4ed214ca57',
'Of Software and Strive at Strava',
'Hey! My name is Nikhil and I spent this past summer as an iOS Software Engineering Intern at Strava.
I’ll be graduating from UC Berkeley this year with a major in Data Science and emphasis in human-computer interaction. I used Strava throughout my days as a runner in high school and reinstalled the app last year on a whim. Like many of Strava’s athletes, I was in search of motivation and a sense of community. In a world of endless scrolling and farming clicks, Strava seemed to offer something different. Eleven weeks of fulfilling work, opportunities for personal and professional growth, as well as an electrifying culture, showed me that my intuition held true not just for Strava’s athletes, but for the company’s employees as well.
The engineers of #sfintern
I was placed on the Growth team, which focuses primarily on registering, onboarding, and motivating the platform’s newest athletes. I’ve been able to work on projects ranging from increasing user notification accept rates to preparing the application for iOS 13; I also helped build an entirely new surface for content discovery.
Being thrust into the iOS codebase was both terrifying and exhilarating. With more commits than hours of sleep I’ve gotten in the last decade, as well as an ever-shrinking set of internal and external dependencies, it was difficult not to feel overwhelmed. My sidekick Tim, who I’ve come to refer to as “sensei,” started me off with small tasks like UI tweaks and copy changes. The Growth team uses frequent A/B testing to determine the efficacy of changes to the app both minor and major, and I was able to take part in this experimentation by rolling out a variant of the onboarding flow after the experiment had reached statistical significance!
I took on my first large-scale project during Guild Week, a time for Strava’s engineers to plan and execute work designed to eliminate technical debt, boost performance, and update dependencies for their respective platforms. I was tasked with overhauling the Login screen, which was previously written in Objective-C, in Swift using the View Controller-Interactor-Presenter architecture pattern.
*Presenting* the new Login scene during the intern tech talks
The advantage of the pattern is that by separating massive view controllers into three primary components — the display logic, business logic, and presentation logic — developers can easily add and test functionality as the user experience evolves. VIP, or Very Insistent Pain-in-my-ass, came to dominate my life for the next two and a half weeks. At times, progress was painfully slow and I began to question my initial assumptions, finding flaws in my approach as I added Google and Facebook authentication to the screen.
I was essentially struggling to rewrite code a more experienced developer had built years ago. Before their changes are merged into the codebase, software engineers first submit pull requests for other engineers to review and offer feedback on. My PR for the Login scene eventually accumulated 76 comments, and after an iOS engineer Slack-messaged me to ask if I was intent on breaking a Strava record, it became tough to convince myself that I was contributing to my team.
That week, a tech talk by my team’s senior engineering manager, who literally wrote the book on developing production-quality web applications, delved into the imposter syndrome. Defined as “feelings of inadequacy that persist despite evident success,” the phenomenon can often cripple the confidence of even the most decorated professionals. The presentation was illuminating, and after voicing my concerns to my manager during our one-on-one that afternoon, I was assured that my confidence would grow day-by-day. I dedicated myself to working on more focused portions of the project, securing feedback more frequently. Before I knew it, the unit tests were completed and the final pull request approved. The work paid dividends during the final weeks of my internship when the Growth team began implementing Apple’s proprietary sign-in solution, creatively titled Sign In with Apple. I then refactored Strava’s Sign Up scene in VIP, reducing the development cost necessary to implement the new authentication method client-side. I was able to quickly build a new Interface Builder file according to the specifications of our designer without worrying about overwriting existing IBOutlets and IBActions for a view controller vital to new athlete registration.
A chart I made to show the unidirectional flow of control in the Explore feed
The onboarding refactorization paved the way for the summer’s main event: the Explore tab. My team embarked upon a seven-week sprint to build a brand-new screen designed to bring athletes the best content on Strava, including popular segments, nearby clubs, activity lookbacks and photos from friends. I was able to provide feedback during the design stage, build the view controller to house the feed, and work on entry modules for the tab. A seven-week sprint for a project as large as Explore was unheard of, but my manager Jason had faith we could do it. I jumped headfirst into the work, utilizing my newfound knowledge of VIP to build an animated loading state and error handling for the modular feed. One of my fondest memories at Strava came during the seventh week of the project when I stayed late one Thursday evening — much to the chagrin of my manager — grinding through updating a server-driven module in the feed to fit our designer’s specifications. The dynamic was electric and in that moment I truly felt like part of the team.
Despite the Growth Activation team’s decision to focus exclusively on Explore for the duration of the seven-week sprint, I was encouraged to participate in this quarter’s edition of Jams, the company’s cross-functional hackathon. I met and worked with engineers on other vertical teams, eventually settling on the task of building a richer, more fulfilling ski activity experience. I had the opportunity to lead employees through a brainstorming and wireframing session as well as gain server-side experience customizing the Activity Detail page. I presented my work in front of the company and shared the stage with Yandong, Strava’s CTO, to talk about a project we worked on to improve activity-specific routes with techniques like matrix factorization and simple recommender systems.
An action shot taken by the talented Ben de Jesus during the kickball game
Working with the Growth team and with cross-platform engineers during Jams are just two examples of how interpersonal relationships at Strava have made my internship experience one I’ll never forget. From day one, interns were welcomed with open arms into the company’s culture of strive, encouraged to pursue our interests both in and out of the office. Whether at a viciously competitive kickball game against our managers at the Summer Picnic to runs through the Marin Headlands, the friendships quickly became as fulfilling as the work itself. I’m grateful for all I’ve learned, not simply about architecture patterns and A/B testing, but about myself as well. Strava’s given me a taste of what it means to be a software engineer and the company’s given me a network of people I’m sure to keep in touch with for years to come.',
'Hey! My name is Nikhil and I spent this past summer as an iOS Software Engineering Intern at Strava.',
 true,
'91b1e88a-fbf9-4d0e-ac31-0272c81aceee', '08139ffb-76f0-4a2e-973f-85218f0d3fc7');
insert into tags_assignements values ('1a335e9d-c74f-44fc-9c65-9b4ed214ca57', '99abcec4-77ac-4a61-b0ec-6f097f0f4928');
insert into tags_assignements values ('1a335e9d-c74f-44fc-9c65-9b4ed214ca57', '84104992-e0c9-42f1-b2cc-aa0e7fee0ce8');
insert into tags_assignements values ('1a335e9d-c74f-44fc-9c65-9b4ed214ca57', '516493fb-32ba-479b-b090-4a90cbad1666');
insert into tags_assignements values ('1a335e9d-c74f-44fc-9c65-9b4ed214ca57', '86bf50fc-916a-4991-9ad3-f88a93c2db88');

insert into users values ('6f38bc04-2654-41c5-b7ec-a71d7475c759', 'paul', 'paul.zenika@mail.com');
insert into categories values ('b5080cc3-0be3-4761-9c35-8c9fc4a16d46', 'Engineering');
insert into tags values ('93258a14-8687-4e50-be42-18badd8dd671', 'sport');

insert into articles values ('b44b2d30-07c3-4211-a319-caf5f698fe53',
'You too can strive as a non-traditional intern',
'Hello everyone, I’m Daniel and I came from a non-traditional coding background. I can’t describe how happy I was when I got an offer for a summer internship at Strava! I worked as an iOS intern alongside a team of amazing web, Android, and iOS engineers. Here, I will tell you about some of the work I did, the culture of Strava, and how much fun I had!
In the beginning
Prior to Strava, I had never seen such a large codebase in my life. I was quite lost as I scrolled through the files just to see how things worked. Although I felt slightly overwhelmed, I was also empowered to demonstrate my skills when assigned to build my first feature. In fact, I was already getting my hands in the app within the first week of my internship.
What I worked on
A majority of my work was focused on improving our iOS recording quality through a rework of the current activity uploading service. To achieve this, we’re beginning to transition our uploads from using JSON to the more reliable FIT file format. I built a new pipeline for iOS and WatchOS that supports FIT uploads with a status menu to validate uploads and handle errors. During that period, I had to write my first unit test to ensure my work can be connected to my colleagues in the upcoming weeks. This is where I struggled most, as I tried to conceptualize how to properly test my code. It was initially difficult to grasp, but I stayed persistent and worked closely with my mentor to develop my thought process. As the picture became clearer, I realized that this was a necessary hurdle as it gave me a new perspective in developing cleaner code.
Both devices share the same uploader
Mentorship
In the process of building things, I received some of the best mentorships I’ve ever experienced at Strava. My mentor Tom challenged me to not only navigate and break down problems but also to think abstractly as a software engineer. Though this happens once a while:
Yes, there were a couple of phases where some of the concepts took time to sync. I think this is what they call coder’s block? Fortunately, I had guidance from my mentor where everything becomes more digestible. I felt a great sense of satisfaction as I started to apply my knowledge. Only to discover I got merge conflicts:
My worries prior to the internship
Rest assure, the codebase didn’t catch fire!
I’ve always been afraid of merge conflicts, mainly because I didn’t know how to resolve them properly in the past. This is also why gitting the support from my mentor and a team of iOS engineers made such a difference. One of the obstacles was preparing the app to support the upcoming iOS13 release. For me, this was tricky to visualize because I had to merge a segment of my code from the beta version into the current codebase. This is when I was introduced with the tools such as FileMerge and Sourcetree that made resolving conflicts a lot easier. Moreover, my mentor patiently drew out branches and challenged me to explain the problem, which helped me navigate to the solution. Now, I’m embracing each obstacle, as I know it will build my experience in solving new problems.
The culture
What gets me excited every morning is knowing how fired up my colleagues are when they come to work. I had a conversation with one of the iOS engineers where I mentioned that I had never worked on any codebase as complex as your app. He immediately corrected me and said, “You mean our app”, and reassured me that I’m part of the team now. Those words stuck with me, as I now feel empowered to take ownership of the app. It also goes to show that the camaraderie, one of Strava’s core values, is embraced throughout the entire company.
Fun of being an intern
Aside from building things, I had a ton of fun when it came to participating in the many activities the internship has to offer. One of which was being a judge of the guacamole challenge. In the photo below, each dip offers a different flavor profile.
I’ll let the plate speak for itself
Group runs and workouts
Strava has a tradition of Workout Wednesdays, which is one of the toughest workouts I’ve done in my life. Yet, somehow through the sweat, exhaustion, and maybe tears, Wednesday grew into one of my favorite days of the week. Once in a while, I get to meet someone new, where we chat about our common interest in coffee, boba, and puppies.
Conclusion
How do you know you’re thriving? When it doesn’t feel like work. I came here to build something that I’m proud of and I did it. You will get a lot of support as an intern here. In terms of my thinking process, code quality, and working with people with a different technical background, I feel like I’ve grown a lot through this internship.
Lastly, I really want to thank Tom, Jason, Kyle, Matt, Rod, Merty, and many more of you for supporting my growth as an iOS Engineer!',
'Hello everyone, I’m Daniel and I came from a non-traditional coding background. I can’t describe how happy I was when I got an offer for a summer internship at Strava!',
true,
'b5080cc3-0be3-4761-9c35-8c9fc4a16d46',
'c8b3a325-5af4-43c3-87d6-14bc94a7b1df');

insert into tags_assignements values ('b44b2d30-07c3-4211-a319-caf5f698fe53', '99abcec4-77ac-4a61-b0ec-6f097f0f4928');
insert into tags_assignements values ('b44b2d30-07c3-4211-a319-caf5f698fe53', '84104992-e0c9-42f1-b2cc-aa0e7fee0ce8');
insert into tags_assignements values ('b44b2d30-07c3-4211-a319-caf5f698fe53', '516493fb-32ba-479b-b090-4a90cbad1666');
insert into tags_assignements values ('b44b2d30-07c3-4211-a319-caf5f698fe53', '86bf50fc-916a-4991-9ad3-f88a93c2db88');
