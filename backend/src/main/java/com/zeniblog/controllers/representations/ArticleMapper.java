package com.zeniblog.controllers.representations;

import com.zeniblog.services.domain.Article;
import com.zeniblog.services.domain.Tag;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class ArticleMapper {

    public ArticleShortRepresentaiton mapToShortArticle(Article article) {

        ArticleShortRepresentaiton result = new ArticleShortRepresentaiton();
        result.setId(article.getId());
        result.setTitle(article.getTitle());
        result.setArticleAbstract(article.getArticleAbstract());
        result.setCategorie_id(article.getCategorie().getId());

        //tagList
        List<String> taglist = new ArrayList<>();
        for (Tag tag : article.getTagList()) {
            taglist.add(tag.getId());
        }
        result.setTagList(taglist);



        return result;
    }

    public ArticleFullRepresentation mapToFullArticle(Article article) {

        ArticleFullRepresentation result = new ArticleFullRepresentation();
        result.setId(article.getId());
        result.setTitle(article.getTitle());
        result.setArticleAbstract(article.getArticleAbstract());
        result.setContent(article.getContent());
        result.setAuthor_id(article.getAuthor().getId());
        result.setCategorie_id(article.getCategorie().getId());
        result.setStatus(article.isStatus());

        //tagList
        List<String> taglist = new ArrayList<>();
        for (Tag tag : article.getTagList()) {
            taglist.add(tag.getId());
        }
        result.setTagList(taglist);

        return result;
    }
}
