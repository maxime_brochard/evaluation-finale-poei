package com.zeniblog.services;

import com.zeniblog.repository.tagRepository;
import com.zeniblog.services.domain.Tag;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Optional;

@Component
public class tagService {
    private tagRepository tagRepo;

    public tagService(tagRepository tagRepo) {
        this.tagRepo = tagRepo;
    }

    public List<Tag> getAllTags(){
        return (List<Tag>) this.tagRepo.findAll();
    }

    public Optional<Tag> getTagById(String id){
        return this.tagRepo.findById(id);
    }
}
