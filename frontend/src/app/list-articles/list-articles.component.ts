import {Component, EventEmitter, Input, OnInit} from '@angular/core';
import {ZBlogService} from '../service/ZBlogService';
import {Observable, of} from 'rxjs';
import {Article} from '../model/Article';

@Component({
  selector: 'app-list-articles',
  templateUrl: './list-articles.component.html',
  styleUrls: ['./list-articles.component.css']
})
export class ListArticlesComponent implements OnInit {
  articles: Observable<Article[]>;
  @Input() forceChange: EventEmitter<boolean>;

  constructor(private zService: ZBlogService) {
  }

  ngOnInit() {
    this.updateList();
  }

  updateList() {
    this.articles = this.zService.getArticles();
  }
}
