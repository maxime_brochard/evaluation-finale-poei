package com.zeniblog.controllers.representations;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.zeniblog.services.domain.Tag;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArticleShortRepresentaiton {
    private String id;
    private String title;
    private String articleAbstract;
    private String categorie_id;
    private List<String> tagList;

    public String getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(String categorie_id) {
        this.categorie_id = categorie_id;
    }

    public List<String> getTagList() {
        return tagList;
    }

    public void setTagList(List<String> tagList) {
        this.tagList = tagList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArticleAbstract() {
        return articleAbstract;
    }

    public void setArticleAbstract(String articleAbstract) {
        this.articleAbstract = articleAbstract;
    }
}
