package com.zeniblog.repository;

import com.zeniblog.services.domain.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface articleRepository extends JpaRepository<Article, String> {
}
