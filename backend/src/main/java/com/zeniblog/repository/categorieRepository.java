package com.zeniblog.repository;

import com.zeniblog.services.domain.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface categorieRepository extends JpaRepository<Categorie, String> {
}
