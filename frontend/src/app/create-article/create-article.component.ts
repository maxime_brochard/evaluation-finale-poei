import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NewArticle} from '../model/NewArticle';
import {ZBlogService} from '../service/ZBlogService';
import {Tag} from '../model/Tag';
import {Article} from '../model/Article';
import {Categorie} from '../model/Categorie';
import {User} from '../model/User';


@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  @Input() showMe: boolean;
  title: string;
  content: string;
  abstract: string;
  status: boolean;
  categorie: string;
  tagIdList: string[];
  tag: string;
  user: string;
  @Output() cancel = new EventEmitter();
  @Output() sumbittedArticle = new EventEmitter();
  tagList: Tag[] = [];
  categorieList: Categorie[] = [];
  userList: User[] = [];

  constructor(private zService: ZBlogService) {
  }

  ngOnInit() {
    this.zService.getAllTags().subscribe((tagList) => {
      this.tagList = tagList;
    });

    this.zService.getAllCategories().subscribe((categorieList) => {
      this.categorieList = categorieList;
    });

    this.zService.getAllUsers().subscribe((userList) => {
      this.userList = userList;
    });
  }

  cancelArticleCreation() {
    this.cancel.emit();
  }

  createArticle() {
    const listOfTags = [this.tag];
    const toCreate = new NewArticle(this.title, this.user, this.abstract, this.content, this.status, this.categorie, listOfTags);

    this.zService.createArticle(toCreate).subscribe(
      article => {
        if (article) {
          this.sumbittedArticle.emit(true);
          this.cancel.emit();
        } else {
          this.sumbittedArticle.emit(false);
          this.cancel.emit();
        }
      }
    );
  }
}
