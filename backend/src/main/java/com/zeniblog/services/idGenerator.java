package com.zeniblog.services;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class idGenerator {

    public String generateNewId() {
        String id = UUID.randomUUID().toString();
        return id;
    }
}

