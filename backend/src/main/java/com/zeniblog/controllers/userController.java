package com.zeniblog.controllers;

import com.zeniblog.services.UserService;
import com.zeniblog.services.domain.Categorie;
import com.zeniblog.services.domain.Tag;
import com.zeniblog.services.domain.User;
import com.zeniblog.services.tagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/users")
public class userController {
    public UserService userService;

    @Autowired
    public userController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    List<User> getAllCategories() {
        return this.userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getOneUser(@PathVariable("id") String id){
        return this.userService.getUserById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
