import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../model/Article';
import {ZBlogService} from '../service/ZBlogService';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Tag} from '../model/Tag';
import {switchMap} from 'rxjs/operators';
import {User} from '../model/User';
import {Subscription} from 'rxjs';
import {Categorie} from '../model/Categorie';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @Input() article: Article;
  taglist: Tag[] = [];
  author: User;
  categorie: Categorie;
  deleted: boolean;

  constructor(private zService: ZBlogService,
              private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const articleId = this.route.snapshot.paramMap.get('id');
    this.zService.getArticle(articleId).subscribe(article => {
      this.article = article;
      this.taglist = this.getTagList(article);

      this.zService.getUser(article.author_id).subscribe(result => {
        this.author = result;
      });

      this.zService.getCategorie(article.categorie_id).subscribe(result => {
        this.categorie = result;
      });
    });
  }

  getTagList(article: Article): Tag[] {
    const list: Tag[] = [];
    for (let i = 0; i < article.tagList.length; i++) {
      this.zService.getTag(article.tagList[i]).subscribe(tag => {
        list.push(tag);
      });
    }
    return list;
  }

  deletedToogle() {
    if (this.deleted) {
      this.deleted = false;
    }
    if (!this.deleted) {
      this.deleted = true;
    }

  }
}
