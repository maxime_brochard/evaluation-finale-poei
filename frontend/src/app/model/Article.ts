export class Article {
id: string;
  title: string;
  content: string;
  articleAbstract: string;
  status: boolean;
  author_id: string;
  categorie_id: string;
  tagList: string[];

}
