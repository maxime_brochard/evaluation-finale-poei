package com.zeniblog.controllers;

import com.zeniblog.controllers.representations.ArticleShortRepresentaiton;
import com.zeniblog.services.categorieService;
import com.zeniblog.services.domain.Categorie;
import com.zeniblog.services.domain.User;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/categories")
public class categorieController {
    public categorieService categorieService;

    @Autowired
    public categorieController(com.zeniblog.services.categorieService categorieService) {
        this.categorieService = categorieService;
    }

    @GetMapping
    List<Categorie> getAllCategories() {
        return this.categorieService.getAllCategories();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Categorie> getOneCategorie(@PathVariable("id") String id){
        return this.categorieService.getCategorieById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
