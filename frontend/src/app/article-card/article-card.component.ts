import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../model/Article';
import {ZBlogService} from '../service/ZBlogService';
import {Tag} from '../model/Tag';
import {colors} from '@angular/cli/utilities/color';
import {Categorie} from '../model/Categorie';

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.css']
})
export class ArticleCardComponent implements OnInit {
  @Input() article: Article;
  tagList: Tag[] = [];
  categorie: Categorie;

  constructor(private zService: ZBlogService) {
  }

  ngOnInit() {
    this.tagList = this.getTagList();

    this.zService.getCategorie(this.article.categorie_id).subscribe(result => {
      this.categorie = result;
    });
  }

  getTagList(): Tag[] {
    const list: Tag[] = [];
    for (const tagId of this.article.tagList) {
      this.zService.getTag(tagId).subscribe((r) => {
        list.push(r);
      });
    }
    return list;
  }


  randomColorGenerator(type: string): string {
    const baba: string[] = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'];
    const randomColor = baba[Math.floor(Math.random() * baba.length)];
    return `${type}-${randomColor}`;
  }
}
