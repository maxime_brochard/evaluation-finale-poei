package com.zeniblog.controllers;

import com.zeniblog.controllers.representations.ArticleShortRepresentaiton;
import com.zeniblog.services.domain.Tag;
import com.zeniblog.services.tagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/tags")
public class tagController {
    public tagService tagService;

    @Autowired
    public tagController(tagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    List<Tag> getAllArticles() {
        return this.tagService.getAllTags();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Tag> getOneTag(@PathVariable("id") String id){
        return this.tagService.getTagById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
