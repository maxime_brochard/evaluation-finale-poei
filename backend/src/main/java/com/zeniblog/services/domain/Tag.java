package com.zeniblog.services.domain;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tags")
public class Tag {
    @Id
    private String id;
    private String name;

    @ManyToMany(mappedBy = "tagList", fetch = FetchType.EAGER)
    Set<Article> assignedArticles;

    public Tag() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tag(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
