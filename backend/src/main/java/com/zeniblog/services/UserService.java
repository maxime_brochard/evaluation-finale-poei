package com.zeniblog.services;

import com.zeniblog.repository.userRepository;
import com.zeniblog.services.domain.Categorie;
import com.zeniblog.services.domain.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserService {
    private userRepository userRepo;

    public UserService(userRepository userRepo) {
        this.userRepo = userRepo;
    }

    public List<User> getAllUsers(){
        return this.userRepo.findAll();
    }

    public Optional<User> getUserById(String id){
        return this.userRepo.findById(id);
    }

    public User getUserIfIdExist(String id){
        Optional<User> maybeUser = this.getUserById(id);
        if (maybeUser.isPresent()){
            return new User(maybeUser.get().getId(), maybeUser.get().getPseudo(), maybeUser.get().getPseudo());
        }throw new IllegalArgumentException("Invalid process, the Id provided does not correspond to any existent User");
    }
}
