package com.zeniblog.repository;

import com.zeniblog.services.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface tagRepository extends JpaRepository <Tag, String> {

}
