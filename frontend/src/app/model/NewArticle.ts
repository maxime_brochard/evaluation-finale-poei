export class NewArticle {
  title: string;
  author_id: string;
  articleAbstract: string;
  content: string;
  status: boolean;
  categorie_id: string;
  tagList: string[];


  constructor(title: string, author_id: string, articleAbstract: string, content: string, status: boolean, categorie_id: string, tagList: string[]) {
    this.title = title;
    this.author_id = author_id;
    this.articleAbstract = articleAbstract;
    this.content = content;
    this.status = status;
    this.categorie_id = categorie_id;
    this.tagList = tagList;
  }
}
